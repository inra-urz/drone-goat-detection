#!/usr/bin/python3
import sys
import glob
import os.path
import numpy as np
import math
import cv2

from shared.color import *
from shared.filter import *
from shared.keyboard import *
from shared.texture_classification import *
from shared.non_max_suppression import *
from shared.feature_descriptor import *

from threading import Thread
from functools import partial
from natsort import natsorted
from tqdm import tqdm, tqdm

#
filename = '../set/pasture.jpg'
#filename = '../set/18.tif'
#filename = '/run/media/root/Ascomycete/Stage/03-skylan-shot/130417/g2/130417940g2.JPG'
#filename = '/run/media/root/Ascomycete/Stage/03-skylan-shot/170517/g1/1705171700g1.JPG'
#filename = '/run/media/root/Ascomycete/Stage/03-skylan-shot/150517/g1/1505171020g1.JPG'
filename = '/run/media/root/Sarcosphaera/06-Backup/stage/03-skylan-shot/170517/g1/1705171700g1.JPG'

image = cv2.imread(filename)
config = {}

params = cv2.SimpleBlobDetector_Params()
params.minThreshold = 10;
params.maxThreshold = 200;
params.filterByArea = False
params.filterByCircularity = False
params.filterByConvexity = False
params.filterByInertia = False
params.filterByColor = False

detector = cv2.SimpleBlobDetector_create(params)

classification = TextureClassification()
classification.load_learned_models()

def classification_func(image, result, index):
    area = cv2.resize(image, classification.fixed_size)
    area = equalize_intensity(area)
    fd = features_descriptors(area).reshape(1,-1)
    classes = [model[1].predict(fd)[0] for model in classification.models]
    result[index] = int(np.mean(classes) > 0.5)
    #cv2.imwrite('../set/trash/2-' + str(index) + '.jpg', area)
pass

def detecting_blobs(transformed):
    draw = image.copy()
    colors = [(255,0,0), (0,0,255)]
    
    bbox_features = multiplane_blob_detection(detector, transformed, [12,64], 4)
    
    bbox_overlaps = 0.5
    indices = non_max_suppression(np.array(bbox_features, dtype=np.float32), bbox_overlaps)
    bbox_features = [bbox_features[i] for i in indices]
    bbox_features = np.int32(bbox_features)
    
    threads = [None] * len(bbox_features)
    results = [None] * len(bbox_features)
    
    for i in tqdm(range(len(threads))):
        x1,y1,x2,y2 = bbox_features[i]
        area = image[y1:y2, x1:x2]
        threads[i] = Thread(
            target=classification_func,
            args=(area, results, i)
        )
        threads[i].start()
    pass
    
    for i in tqdm(range(len(threads))):
        threads[i].join()
        x1,y1,x2,y2 = bbox_features[i]
        if results[i] == 0:
            cv2.rectangle(draw, (x1, y1), (x2, y2), colors[results[i]], 2)
    pass
    
    cv2.imshow('Original', draw)
pass

def main():
    cv2.namedWindow('Original', cv2.WINDOW_NORMAL)
    
    config['blur_size'] = [5,8]
    config['sharpen'] = True
    config['remove_yellow'] = True
    config['bilateral'] = True
    config['remove_green_thresh'] = [10, 5]
    config['remove_green_max']  = [255, 100]
    
    transformed = image_processing(image, config, True)
    detecting_blobs(transformed)
    
    while not waitOrExit(0): pass
pass

if __name__ == "__main__":
    main()