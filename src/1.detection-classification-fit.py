#!/usr/bin/python3
import sys
import h5py
import pickle
import numpy as np
import os
import glob
import cv2

from shared.texture_classification import *
from shared.feature_descriptor import *

from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.model_selection import KFold, StratifiedKFold
from sklearn.metrics import confusion_matrix, accuracy_score, classification_report
from sklearn.externals import joblib

from random import *
from matplotlib import pyplot as plt
from tqdm import tqdm

def main():
    classification = TextureClassification()

    global_features = classification.load_h5f_data()
    global_labels = classification.load_h5f_label()

    # verify the shape of the feature vector and labels
    print("[STATUS] features shape: {}".format(global_features.shape))
    print("[STATUS] labels shape: {}".format(global_labels.shape))
    print("[STATUS] training started...")

    # fit the training data to the model
    models = classification.build_learning_models()
    for name, model in tqdm(models):
        model.fit(global_features, global_labels)
        joblib.dump(model, '../result/texture-classifier-' + name + '.pkl') 
    pass
    
    print(models)
pass

# filter all the warnings
import warnings

if __name__ == "__main__":
    warnings.filterwarnings('ignore')
    main()