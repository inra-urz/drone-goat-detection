#!/usr/bin/python3
import sys
import h5py
import os
import glob
import cv2

import numpy as np
import pandas as pd

from shared.texture_classification import *
from shared.feature_descriptor import *

from sklearn.ensemble import VotingClassifier
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.model_selection import KFold, StratifiedKFold
from sklearn.metrics import confusion_matrix, accuracy_score, classification_report
from sklearn.externals import joblib

from random import *
from matplotlib import pyplot as plt
from tqdm import tqdm

import warnings
warnings.filterwarnings('ignore')

def discrete_matshow(name, fname, data):
    #get discrete colormap
    #cmap = plt.get_cmap('RdBu', np.max(data)-np.min(data)+1)
    cmap = plt.get_cmap('seismic')
    
    fig = plt.figure()
    fig.suptitle(name)
    ax = fig.add_subplot(111)
    
    # set limits .5 outside true range
    mat = ax.matshow(data,cmap=cmap,vmin = np.min(data)-.5, vmax = np.max(data)+.5)
    #tell the colorbar to tick at integers
    cax = plt.colorbar(mat, ticks=np.arange(-1,2))
    
    fig.savefig(fname)
    plt.show()
pass

def main():
    classification = TextureClassification()
    #classification.load_learned_models()
    
    print('loading data')
    
    global_features = classification.load_h5f_data()
    global_labels = classification.load_h5f_label()
        
    print('converting to dataframe')
    
    original = np.array(global_features)
    
    df = np.array(original.copy())
    df = np.array(pd.DataFrame(df).corr())
    
    discrete_matshow('FeatureCorrelation', '../result/texture-feature-correlation.jpg', df)
    
    #normalized = np.array(original.copy())
    #for i in range(normalized.shape[0]):
    #    normalized[i,:] -= min(normalized[i,:])
    #    normalized[i,:] /= max(normalized[i,:])
    #normalized = np.array(pd.DataFrame(normalized).corr())
    #
    #discrete_matshow('FeatureCorrelation', '../result/classification-normalized-correlation.jpg', df)
pass

if __name__ == "__main__":
    main()