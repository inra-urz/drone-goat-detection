#!/usr/bin/python3
import sys
import glob
import os.path
import numpy as np
import math
import cv2

from shared.filter import *
from shared.keyboard import *
from shared.texture_classification import *
from shared.non_max_suppression import *
from shared.feature_descriptor import *

from threading import Thread
from functools import partial
from natsort import natsorted
from tqdm import tqdm, tqdm

filename = '../set/pasture.jpg'
#filename = '../set/18.tif'
#filename = '/run/media/root/Sarcosphaera/06-Backup/stage/03-skylan-shot/130417/g2/130417940g2.JPG'
#filename = '/run/media/root/Sarcosphaera/06-Backup/stage/03-skylan-shot/170517/g1/1705171700g1.JPG'

image = cv2.imread(filename)
config = {}

def update_config(params, index, x):
    config[params][index] = x
    transformed = image_processing(image, config, True)
    cv2.imshow('Transformed', transformed)
pass

def main():
    cv2.namedWindow('Original', cv2.WINDOW_NORMAL)
    cv2.namedWindow('Transformed', cv2.WINDOW_NORMAL)
    cv2.namedWindow('Config', cv2.WINDOW_NORMAL)
    
    config['blur_size'] = [5, 8]
    config['sharpen'] = True
    config['remove_yellow'] = True
    config['bilateral'] = True
    config['remove_green_thresh'] = [0, 5]
    config['remove_green_max']  = [255, 100]
    
    transformed = image_processing(image, config, True)
    
    cv2.imshow('Original', image)
    cv2.imshow('Transformed', transformed)
    
    cv2.createTrackbar('bs1', 'Config', config['blur_size'][0], 10, partial(update_config, 'blur_size', 0))
    cv2.createTrackbar('bs2', 'Config', config['blur_size'][1], 10, partial(update_config, 'blur_size', 1))
    cv2.createTrackbar('rgt1', 'Config', config['remove_green_thresh'][0], 255, partial(update_config, 'remove_green_thresh', 0))
    cv2.createTrackbar('rgt2', 'Config', config['remove_green_thresh'][1], 255, partial(update_config, 'remove_green_thresh', 1))
    cv2.createTrackbar('rgm1', 'Config', config['remove_green_max'][0], 255, partial(update_config, 'remove_green_max', 0))
    cv2.createTrackbar('rgm2', 'Config', config['remove_green_max'][1], 255, partial(update_config, 'remove_green_max', 1))
    
    while not waitOrExit(0): pass
pass

if __name__ == "__main__":
    main()