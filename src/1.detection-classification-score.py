#!/usr/bin/python3
import sys
import h5py
import pickle
import numpy as np
import os
import glob
import cv2

from shared.texture_classification import *
from shared.feature_descriptor import *

from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.model_selection import KFold, StratifiedKFold
from sklearn.metrics import confusion_matrix, accuracy_score, classification_report
from sklearn.externals import joblib

from random import *
from matplotlib import pyplot as plt
from tqdm import tqdm

def render_graph(results, names, test_perk):
    # boxplot algorithm comparison
    fig = plt.figure()
    fig.suptitle('Machine Learning algorithm comparison - Texture classification')
    ax = fig.add_subplot(111)
    
    plt.ylim(0.95, 1.0)
    plt.yticks(np.arange(0.90, 1.0, step=0.005))
    plt.boxplot(results)
    
    ax.set_xticklabels(names)
    
    plt.savefig(
        '../result/texture-classification-learn-' +
        str(100-test_perk) +'-' + str(test_perk) + '.jpg'
    )
    plt.close()
pass

def main():
    classification = TextureClassification()
    models = classification.build_learning_models()

    # variables to hold the results and names
    results = []
    names = []
    scoring = "accuracy"

    global_features = classification.load_h5f_data()
    global_labels = classification.load_h5f_label()

    # verify the shape of the feature vector and labels
    print("[STATUS] features shape: {}".format(global_features.shape))

    for test_perk in [80]:
        test_size = int(global_features.shape[0] * test_perk / 100)
        seed = 0

        # split the training and testing data
        trainData, testData, trainLabels, testLabels = train_test_split(
            np.array(global_features),
            np.array(global_labels),
            test_size=test_size,
            random_state=seed
        )

        print("[ INFO ] Train : {}".format(trainData.shape))
        print("[ INFO ] Test  : {}".format(testData.shape))

        # 10-fold cross validation
        for name, model in tqdm(models):
            kfold = KFold(n_splits=10, random_state=7)
            cv_results = cross_val_score(model, trainData, trainLabels, cv=kfold, scoring=scoring)
            results.append(cv_results)
            names.append(name)
            msg = "[ DONE ] %s: %f (%f)" % (name, cv_results.mean(), cv_results.std())
            print(msg)
        pass

        render_graph(results, names, test_perk)
    pass
pass

# filter all the warnings
import warnings

if __name__ == "__main__":
    warnings.filterwarnings('ignore')
    main()