#!/usr/bin/python3#!/usr/bin/python3
"""
extract features from learning folder and save the output to
h5f_data = h5py.File('../result/data.h5', 'w')
h5f_label = h5py.File('../result/labels.h5', 'w')
"""
import sys
import mahotas
import cv2
import os
import h5py

import numpy as np

from shared.texture_classification import *
from shared.feature_descriptor import *
from shared.color import *

from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import MinMaxScaler

from tqdm import tqdm
from glob import glob

def main():
    classification = TextureClassification()

    global_features = []
    labels = []
    
    for training_name in classification.train_labels:
        dir = os.path.join(classification.train_path, training_name)
        current_label = training_name
        
        for file in tqdm(glob(dir + "/*")):
            image = cv2.imread(file)
            image = cv2.resize(image, classification.fixed_size)
            image = equalize_intensity(image)
            
            # stretch contrast + non-linear componant

            labels.append(current_label)
            global_features.append(features_descriptors(image))
        pass
        
        print("[STATUS] processed folder: {}".format(current_label))
    pass

    print("[STATUS] completed Global Feature Extraction...")
    print("[STATUS] feature vector size {}".format(np.array(global_features).shape))
    print("[STATUS] training Labels {}".format(np.array(labels).shape))

    le = LabelEncoder()
    targetNames = np.unique(labels)
    target = le.fit_transform(labels)
    
    print("[STATUS] training labels encoded...")

    # normalize the feature vector in the range (0-1)
    #scaler = MinMaxScaler(feature_range=(0, 1))
    #rescaled_features = scaler.fit_transform(global_features)
    
    print("[STATUS] feature vector normalized...")
    print("[STATUS] target labels: {}".format(target))
    print("[STATUS] target labels shape: {}".format(target.shape))

    # save the feature vector using HDF5
    h5f_data = h5py.File('../result/texture-data.h5', 'w')
    h5f_data.create_dataset('dataset_1', data=np.array(global_features))

    h5f_label = h5py.File('../result/texture-labels.h5', 'w')
    h5f_label.create_dataset('dataset_1', data=np.array(target))

    h5f_data.close()
    h5f_label.close()

    print("[STATUS] end of training..")
pass

if __name__ == "__main__":
    main()