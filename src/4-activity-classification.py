#!/usr/bin/python3
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd 
import graphviz 
import itertools

from sklearn.model_selection import GridSearchCV, train_test_split, cross_val_score
from sklearn.metrics import classification_report, confusion_matrix 
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2

from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC

from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.model_selection import KFold, StratifiedKFold
from sklearn.metrics import confusion_matrix, accuracy_score, classification_report
from sklearn.ensemble import VotingClassifier
from sklearn.externals import joblib
from tqdm import tqdm

def main():
    df = pd.read_csv('../result/goat-properties.csv', sep=';')
    np.set_printoptions(precision=2)

    class_name = ['debout','coucher']
    X = df.drop('name_state', axis=1)
    X = X.drop('name', axis=1)
    X = X.drop('detected', axis=1)
    X = X.drop('status', axis=1)
    y = (df['name_state']*10)
    
    test_size = 30

    X = SelectKBest(chi2, k=3).fit_transform(X, y)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size/100)  

    # --------------------------------------

    results = []
    names = []
    scoring = "accuracy"

    classifier = [
        ('LR',   LogisticRegression(random_state=9)),
        ('LDA',  LinearDiscriminantAnalysis()),
        ('QDA',  QuadraticDiscriminantAnalysis()),
        ('KNN',  KNeighborsClassifier()),
        ('CART', DecisionTreeClassifier(random_state=9)),
        ('XT',   ExtraTreesClassifier(n_estimators=9, min_samples_split=2, random_state=9)),
        ('ADA',  AdaBoostClassifier(n_estimators=9)),
        #('Grad', GradientBoostingClassifier(probability=True, n_estimators=9, learning_rate=1.0, max_depth=2, random_state=9)),
        ('RF',   RandomForestClassifier(n_estimators=9, random_state=9)),
        ('NB',   GaussianNB()),
        ('SVM',  SVC(probability=True,random_state=9)),
        ('MLPC', MLPClassifier(alpha=1))
    ]

    # 10-fold cross validation
    for name, model in tqdm(classifier):
        kfold = KFold(n_splits=10, random_state=7)
        cv_results = cross_val_score(model, X_train, y_train, cv=kfold, scoring=scoring)
        results.append(cv_results)
        names.append(name)
        msg = "[ DONE ] %s: %f (%f)" % (name, cv_results.mean(), cv_results.std())
        joblib.dump(model, '../result/activity-classifier-' + name + '.pkl') 
        print(msg)
    pass

    # boxplot algorithm comparison
    fig = plt.figure()
    fig.suptitle('Machine Learning algorithm comparison - Activity from shape metrics')
    ax = fig.add_subplot(111)
    plt.boxplot(results, sym='')
    ax.set_xticklabels(names)
    
    plt.savefig('../result/activity-classification-learn-' + str(100-test_size) + '-' + str(test_size) + '.jpg')
pass

# filter all the warnings
import warnings

if __name__ == "__main__":
    warnings.filterwarnings('ignore')
    main()