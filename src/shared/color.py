import numpy as np
import math
import cv2
import cython

# --------------------------------------------------------

def sharpen(src):
    """
    sharpen all plane with a convolution
    @src the input texture
    @return the sharpened texture
    """

    kernel = np.array([[-1,-1,-1], [-1,9,-1], [-1,-1,-1]])
    
    return cv2.filter2D(src, -1, kernel)

    pass
# --------------------------------------------------------

def mse(imageA, imageB):
    """
    @return the mean square error between imageA and imageB
    """

    err = np.sum((imageA.astype("float") - imageB.astype("float")) ** 2)
    err /= float(imageA.shape[0]*imageA.shape[1])
    
    return err
pass

# --------------------------------------------------------

def image_log(src, log_snr=255):
    """
    @return the log scale of the image
    c = 255/math.log(1+log_snr)
    log_img = c * (np.log(1 + src))
    """

    c = 255/math.log(1+log_snr)
    log_img = c * (np.log(1 + src))
    
    return log_img.astype('uint8')
pass

# --------------------------------------------------------

def adaptive_histogram_equalization(src, gridsize=32, clip=1.3):
    """
    converto from BGR to LAB
    split planes and apply the Contrast-limited adaptive histogram equalization on the L plane
    then convert back to BGR
    @return the equalized image intensity
    """
    
    lab = cv2.cvtColor(src, cv2.COLOR_BGR2LAB)
    lab_planes = cv2.split(lab)
    clahe = cv2.createCLAHE(clipLimit=clip, tileGridSize=(gridsize,gridsize))
    lab_planes[0] = clahe.apply(lab_planes[0])
    lab = cv2.merge(lab_planes)

    src = cv2.cvtColor(lab, cv2.COLOR_LAB2BGR)
    
    lab = cv2.cvtColor(src, cv2.COLOR_BGR2HSV)
    lab_planes = cv2.split(lab)
    lab_planes[1] = clahe.apply(lab_planes[1])
    lab = cv2.merge(lab_planes)
    
    src = cv2.cvtColor(lab, cv2.COLOR_HSV2BGR)
    
    return src
pass

# --------------------------------------------------------

def equalize_intensity(src):
    """
    convert from RGB to YUV
    apply histogram equalization over the Y plane
    convert back to RGB
    @return the equalized image intensity
    """

    src = cv2.cvtColor(src, cv2.COLOR_RGB2YCrCb)
    src[:,:,0] = cv2.equalizeHist(src[:,:,0])
    src = cv2.cvtColor(src, cv2.COLOR_YCrCb2RGB)
    
    return src
pass

# --------------------------------------------------------

def auto_canny(image, sigma=0.93):
    """
    compute the median of the single channel pixel intensities
    apply automatic Canny edge detection using the computed median
    lower = int(max(0, (1.0 - sigma) * v))
    upper = int(min(255, (1.0 + sigma) * v))
    @return cv2.Canny(image, lower, upper)
    """
    v = np.median(image)

    lower = int(max(0, (1.0 - sigma) * v))
    upper = int(min(255, (1.0 + sigma) * v))

    return cv2.Canny(image, lower, upper)
pass

# --------------------------------------------------------

def adjust_gamma(image, gamma=1.0):
    """
    simple gamma correction:
    invGamma = 1.0/gamma
    @return ((i/255.0) ** invGamma) * 255
    """
    
    invGamma = 1.0 / gamma
    table = np.array([((i / 255.0) ** invGamma) * 255 for i in np.arange(0, 256)])
    return cv2.LUT(image, table.astype("uint8"))
pass

# --------------------------------------------------------

@cython.boundscheck(False)
def saturate(src, thesh=240):
    """
    change the color space to HSV
    set saturation and value to 255
    then change the colospace to BRG
    @src the input texture
    """

    src = cv2.cvtColor(src, cv2.COLOR_RGB2HSV)
    src[:,:,1] = np.maximum(thesh, src[:,:,1])
    src[:,:,2] = np.maximum(thesh, src[:,:,2])
    src = cv2.cvtColor(src, cv2.COLOR_HSV2RGB)
    
    return src
pass
    
# --------------------------------------------------------

@cython.boundscheck(False)
def desaturate(src):
    """
    change the color space from RGB to HSV
    set saturation to 0
    then change the colospace to RGB
    @src the input texture
    """

    src = cv2.cvtColor(src, cv2.COLOR_RGB2HSV)
    src[:,:,1].fill(0)
    src = cv2.cvtColor(src, cv2.COLOR_HSV2RGB)
    
    return src
pass
    
# --------------------------------------------------------

@cython.boundscheck(False)
def value(src):
    """
    change the color space from RGB to HSV
    set hue and satuation to 255
    then change the colospace to RGB
    @src the input texture
    """

    src = cv2.cvtColor(src, cv2.COLOR_RGB2HSV)
    src[:,:,0] = np.maximum(240, src[:,:,2])
    src[:,:,1].fill(255)
    src = cv2.cvtColor(src, cv2.COLOR_HSV2RGB)
    
    return src
pass

# --------------------------------------------------------

@cython.boundscheck(False)
def remove_yellow(src):
    """
    change the color space from RGB to YCrCb
    set the Yellow plane to 0
    then change the color space to RGB
    @src the input texture
    """

    src = cv2.cvtColor(src, cv2.COLOR_RGB2YCrCb)
    src[:,:,1].fill(0)
    src = cv2.cvtColor(src, cv2.COLOR_YCrCb2RGB)
    
    return src
pass
    
# --------------------------------------------------------

@cython.boundscheck(False)
def remove_green_major(src, thresh = 20, maximum = 200):
    """
    set all pixel to (0,0,0) when the green is superior to one other component
    @src the input texture
    @return G+thresh > max(B,R) or G > max, to white color
    """
    
    upper = src[:,:,1] > maximum
    upper = upper.astype('uint8')
    
    threshold = np.int32(src[:,:,1]+thresh)
    threshold = np.maximum(src[:,:,0], src[:,:,2]) - threshold
    threshold = np.maximum(threshold, 0)
    threshold = threshold.astype('uint8')
    
    mask = cv2.bitwise_or(upper, threshold)
    
    return cv2.bitwise_and(src, src, mask=mask)
pass
    
# --------------------------------------------------------

@cython.boundscheck(False)
def remove_blue_major(src, thresh = 20, maximum = 200):
    """
    set all pixel to (0,0,0) when the green is superior to one other component
    @src the input texture
    @return G+thresh > max(B,R) or G > max, to white color
    """
    
    upper = src[:,:,1] > maximum
    upper = upper.astype('uint8')
    
    threshold = np.int32(src[:,:,0]+thresh)
    threshold = np.maximum(src[:,:,1], src[:,:,2]) - threshold
    threshold = np.maximum(threshold, 0)
    threshold = threshold.astype('uint8')
    
    mask = cv2.bitwise_or(upper, threshold)
    
    return cv2.bitwise_and(src, src, mask=mask)
pass

# --------------------------------------------------------

@cython.boundscheck(False)
def get_red_major(src, thresh = 20, maximum = 200):
    """
    discard all pixel wich his non red value is compared to the red
    the discarded pixel get the black color [0,0,0]
    @src the input texture
    @return R+thresh < min(B,G) or R < max
    """
    
    upper = src[:,:,2] < maximum
    upper = upper.astype('uint8')
    
    threshold = np.int32(src[:,:,2]-thresh)
    threshold = threshold - np.minimum(src[:,:,0], src[:,:,1])
    threshold = np.maximum(threshold, 0)
    threshold = threshold.astype('uint8')
    
    mask = cv2.bitwise_or(upper, threshold)
    
    return cv2.bitwise_and(src, src, mask=mask)
pass

# --------------------------------------------------------

@cython.boundscheck(False)
def is_green_major(src, thresh = 20, maximum = 200):
    """
    calculate the avarage color of the given texture
    @return g+thresh > std::max(r, b) || g > max
    """

    h = src.shape[0]
    w = src.shape[1]
    
    r = float(src[:,:,2].sum())
    g = float(src[:,:,1].sum())
    b = float(src[:,:,0].sum())
    
    return g+thresh > max(r, b) or g/w/h > maximum;
pass

# --------------------------------------------------------

def get_background_mask(src, bg = (64,64,64), erode_size = 10):
    """
    create a background substraction based on the color of the background
    the background should be black without garbage artifacts
    this function apply a thresholding to @src,
    all pixels with the values < to the background color @bg (64,64,64)
    ie: pixel < (64,64,64) implies the value (0,0,0) and (255,255,255) otherwise
    a dilatation morphological ellipitic operation is finally applied to the mask (remove some artifacts)
    @return the corresponding mask
    """
    
    r = src[:,:,0] < bg[0]
    g = src[:,:,1] < bg[1]
    b = src[:,:,2] < bg[2]

    r = r.reshape((r.shape[0], r.shape[1], 1)).astype('uint8')
    g = g.reshape((r.shape[0], r.shape[1], 1)).astype('uint8')
    b = b.reshape((r.shape[0], r.shape[1], 1)).astype('uint8')
    
    kernel_size = (erode_size*2+1, erode_size*2+1)
    element_erode = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, kernel_size, (erode_size, erode_size))

    mask = (r & g & b)*255
    mask = cv2.dilate(mask, element_erode, iterations = 3)
    mask = mask.reshape((r.shape[0], r.shape[1], 1)).astype('uint8')
    
    return mask
pass

# --------------------------------------------------------

def normalize(src, coef = 128, ord=2):
    """
    divide each componant by the norm of the corresponding vector
    @ord by default the norm order L2
    @coef is a multiplitor factor after the normalization
    """

    normalized = np.array([
        (v / np.linalg.norm(v, ord=ord) * coef)
        for v in src
    ])
    
    return normalized.reshape(src.shape)
pass

# --------------------------------------------------------

def color_transfer(source, target):
    """
    Transfers the color distribution from the source to the target
    image using the mean and standard deviations of the L*a*b* color space.
    This implementation is (loosely) based on to the "Color Transfer
    between Images" paper by Reinhard et al., 2001.
    
    @source OpenCV image in BGR color space (the source image)
    @target OpenCV image in BGR color space (the target image)
    @returns OpenCV image (w, h, 3) NumPy array (uint8)
    """
    
    source = cv2.cvtColor(source, cv2.COLOR_BGR2LAB).astype("float32")
    target = cv2.cvtColor(target, cv2.COLOR_BGR2LAB).astype("float32")

    # compute color statistics for the source and target images
    (lMeanSrc, lStdSrc, aMeanSrc, aStdSrc, bMeanSrc, bStdSrc) = image_stats(source)
    (lMeanTar, lStdTar, aMeanTar, aStdTar, bMeanTar, bStdTar) = image_stats(target)

    # subtract the means from the target image
    (l, a, b) = cv2.split(target)
    l -= lMeanTar
    a -= aMeanTar
    b -= bMeanTar

    # scale by the standard deviations
    l = (lStdTar / lStdSrc) * l
    a = (aStdTar / aStdSrc) * a
    b = (bStdTar / bStdSrc) * b

    # add in the source mean
    l += lMeanSrc
    a += aMeanSrc
    b += bMeanSrc

    # clip the pixel intensities to [0, 255] if they fall outside
    # this range
    l = np.clip(l, 0, 255)
    a = np.clip(a, 0, 255)
    b = np.clip(b, 0, 255)

    # merge the channels together and convert back to the RGB color
    # space, being sure to utilize the 8-bit unsigned integer data
    # type
    transfer = cv2.merge([l, a, b])
    transfer = cv2.cvtColor(transfer.astype("uint8"), cv2.COLOR_LAB2BGR)
    
    # return the color transferred image
    return transfer
pass

# --------------------------------------------------------

def edge_enhancement(src, alpha=0.2):
    kernel = np.array([[-1,-1,-1], [-1,9,-1], [-1,-1,-1]])
    convolved = cv2.filter2D(src, -1, kernel)
    return cv2.addWeighted(src, 1.0-alpha, convolved, alpha, 0.0, src)
pass