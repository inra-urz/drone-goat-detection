#!/usr/bin/python3
import numpy as np
import cv2

def waitOrExit(time=30):
    k = cv2.waitKey(time) & 0xff
    
    if k == 32:
        k = cv2.waitKey() & 0xff
        while k != 32 and k != 27:
            k = cv2.waitKey() & 0xff
            pass
            
    if k == 27:
        return True
        
    return False
pass