import numpy as np
import math
import cv2

# --------------------------------------------------------

def getAspectRatio(cnt):
    """
    It is the ratio of width to height of bounding rect of the object.
    @return width / height
    """

    x,y,w,h = cv2.boundingRect(cnt)
    return float(min(w, h)) / max(w, h)
pass

# --------------------------------------------------------

def getExtent(cnt):
    """
     * Extent is the ratio of contour area to bounding rectangle area.
    """

    area = cv2.contourArea(cnt)
    x,y,w,h = cv2.boundingRect(cnt)
    rect_area = w*h
    return float(area) / rect_area
pass

# --------------------------------------------------------

def getSurfaceSolidity(src, cnt):
    """
    Solidity is the ratio of contour area to its convex hull area.
    this function calculate the ratio between the surface area and the convex hull area
    the area is calculated with the number of white pixel
    @return convex_hull_area / surface_area
    """

    hull = cv2.convexHull(cnt)
    
    mask = np.zeros(src.shape[:2])
    cv2.fillConvexPoly(mask, hull, (255,255,255))
    
    area = cv2.countNonZero(src);
    hull_area = cv2.countNonZero(mask);
    
    return hull_area / area
pass

# --------------------------------------------------------

def getEquivalentDiameter(cnt):
    """
    Equivalent Diameter is the diameter of the circle whose area is same as the contour area.
    """

    area = cv2.contourArea(cnt)
    return np.sqrt(4 * area / np.pi)
pass
    
# --------------------------------------------------------

def drawAxis(src, p, q, colour, scale = 0.2):
    """
    draw an arrow at the position @p to the given direction @p
    @img the texture who is displayed the arrow
    @p the center position
    @q the direction of the arrow
    @colour the arrow colour
    @scale the arrow scale
    """
    
    angle = math.atan2(p[1] - q[1], p[0] - q[0]);
    hypotenuse = math.sqrt((p[1] - q[1]) * (p[1] - q[1]) + (p[0] - q[0]) * (p[0] - q[0]));
    
    # Here we lengthen the arrow by a factor of scale
    q = (int(p[0] - scale * hypotenuse * math.cos(angle)),
         int(p[1] - scale * hypotenuse * math.sin(angle)));
    cv2.line(src, p, q, colour, 1, cv2.LINE_AA);
    
    # create the arrow hooks
    p = (int(q[0] + 9 * math.cos(angle + math.pi / 4)),
         int(q[1] + 9 * math.sin(angle + math.pi / 4)));
    cv2.line(src, p, q, colour, 1, cv2.LINE_AA);
    
    p = (int(q[0] + 9 * math.cos(angle - math.pi / 4)),
         int(q[1] + 9 * math.sin(angle - math.pi / 4)));
    cv2.line(src, p, q, colour, 1, cv2.LINE_AA);
pass

# --------------------------------------------------------

def getOrientation(src):
    """
    @src the texture who is write the axis orientation using @drawAxis
    @return the angle orientation in degree from @pts calculated by mean square approximation
    """
    pass
    
# --------------------------------------------------------

def getExtremPoint(cnt):
    """
    the fourst extrem point of the contours
    @return (leftmost, topmost, rightmost, bottommost)
    """
    
    leftmost   = tuple(cnt[cnt[:,:,0].argmin()][0])
    rightmost  = tuple(cnt[cnt[:,:,0].argmax()][1])
    topmost    = tuple(cnt[cnt[:,:,1].argmin()][0])
    bottommost = tuple(cnt[cnt[:,:,1].argmax()][1])
    
    return (leftmost, topmost, rightmost, bottommost)
pass