#!/usr/bin/python3
import numpy as np
import mahotas
import h5py
import cv2
import os

from threading import Thread
from functools import partial
from skimage.feature import hog

def fd_hu_moments(image, result, index):
    """
    feature-descriptor-1: Hu Moments
    convert the image to grayscale
    @return the computed the hu moment
    """
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    feature = cv2.HuMoments(cv2.moments(image)).flatten()
    
    result[index] = feature
pass

#------------------------------------------------------------

def fd_haralick(image, result, index):
    """
    feature-descriptor-2: Haralick Texture
    convert the image to grayscale
    @return the computed haralick texture feature vector
    """
    
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    feature = mahotas.features.haralick(gray).mean(axis=0)
    
    result[index] = feature
pass

#------------------------------------------------------------

def fd_lbp(image, result, index):
    """
    feature-descriptor-5: LBP Texture
    convert the image to grayscale
    @return the computed lbp texture feature vector
    """
    
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    feature = mahotas.features.lbp(gray, 8, 8, ignore_zeros=False).mean(axis=0)
    
    result[index] = feature
pass

#------------------------------------------------------------

def fd_histogram(image, result, index):
    """
    feature-descriptor-3: Color Histogram
    convert the image to HSV color-space
    compute the color histogram
    normalize the histogram
    @return the histogram
    """
    
    bins = 8
    image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    hist  = cv2.calcHist([image], [0, 1, 2], None, [8, 8, 8], [0, 256, 0, 256, 0, 256])
    cv2.normalize(hist, hist)
    
    result[index] = hist.flatten()
pass

#------------------------------------------------------------
    
def fd_hog(image, result, index, orient=3, pix_per_cell=8, cell_per_block=4):
    """
    # feature-descriptor-4: Histogram of Oriented Gradients
    convert the image to YCrCb color-space
    calculate the Histogram of Oriented Gradients for each plane
    append the features vectors to each one
    @return the first 532 hog features vectors of all planes
    """
    
    (h, w, d) = image.shape
    
    hog_features = []
    pix_per_cell = pix_per_cell
    
    for channel in range(2):
        hog_features.append(hog(
            image[:, :, channel],
            block_norm='L2-Hys',
            orientations=orient,
            pixels_per_cell=(pix_per_cell, pix_per_cell),
            cells_per_block=(cell_per_block, cell_per_block),
            transform_sqrt=True,
            visualise=False,
            feature_vector=False
        ))
    pass
    
    hog_features = np.asarray(hog_features).flatten()
    
    result[index] = hog_features
pass

# --------------------------------------------------------

def fd_stats(image, result, index):
    """
    return [src[0].mean, src[0].std(), ...]
    """
    # compute the mean and standard deviation of each channel
    l, a, b = cv2.split(image)
    
    (lMean, lStd) = (l.mean(), l.std())
    (aMean, aStd) = (a.mean(), a.std())
    (bMean, bStd) = (b.mean(), b.std())

    # return the color statistics
    result[index] = np.array([lMean, lStd, aMean, aStd, bMean, bStd])
pass
    
#------------------------------------------------------------

def features_descriptors(image):
    """
    calculate and return a global features descriptors of the given image
    it's a stack of all fd_function result
    @return np.hstack([features_type]).reshape(1,-1)
    """
    
    # "normalize" the texture
    # also increase the time computation performances by +16%
    #image = cv2.blur(image, (3,3))
    
    color_space = [
        cv2.cvtColor(image, cv2.COLOR_BGR2XYZ),
        cv2.cvtColor(image, cv2.COLOR_BGR2LAB),
        cv2.cvtColor(image, cv2.COLOR_BGR2HSV),
        cv2.cvtColor(image, cv2.COLOR_BGR2YCrCb),
    ]
    
    operations = [
        (fd_stats,      0),
        (fd_stats,      1),
        (fd_stats,      2),
        (fd_stats,      3),
        ###################
        (fd_histogram,  0),
        #(fd_histogram,  1),
        #(fd_histogram,  2),
        #(fd_histogram,  3),
        ###################
        (fd_haralick,   0),
        (fd_lbp,        0),
        (fd_hu_moments, 0),
        (fd_hog,        3),
    ]
    
    threads = [None] * len(operations)
    results = [None] * len(operations)
    
    for i in range(len(threads)):
        threads[i] = Thread(
            target=operations[i][0],
            args=(
                color_space[operations[i][1]],
                results, i
            )
        )
        threads[i].start()
        
    for i in range(len(threads)):
        threads[i].join()
    
    results = np.hstack(results)
    results[np.where(np.isnan(results))] = 0
    results[np.where(np.isinf(results))] = 0
    
    return results
pass