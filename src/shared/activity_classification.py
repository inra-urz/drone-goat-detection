#!/usr/bin/python3
import numpy as np
import mahotas
import h5py
import glob
import cv2
import os

from operator import itemgetter
from skimage.feature import hog

from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC

from sklearn.externals import joblib

class TextureClassification(object):

    def __init__(self):
        self.fixed_size = tuple((32,32))

        self.train_path = "../set/learning/"
        self.test_path = "../set/learning/"
        self.num_trees = 100
        self.test_size = 0.10
        self.seed = 9
        self.models = []

        self.train_labels = os.listdir(self.train_path)
        self.train_labels.sort()

        print(self.train_labels)
    pass
    
    def build_learning_models(self):
        """
        create all the machine learning models
        """
        
        models = []
        
        models.append(('ADA',  AdaBoostClassifier(n_estimators=9)))
        models.append(('CART', DecisionTreeClassifier(random_state=9)))
        models.append(('LDA',  LinearDiscriminantAnalysis()))
        models.append(('LR',   LogisticRegression(random_state=9)))
        models.append(('RF',   RandomForestClassifier(n_estimators=9, random_state=9)))
        models.append(('XT',   ExtraTreesClassifier(n_estimators=9, min_samples_split=2, random_state=9)))
        
        models.append(('QDA',  QuadraticDiscriminantAnalysis()))
        models.append(('NB',   GaussianNB()))
        models.append(('KNN',  KNeighborsClassifier()))
        models.append(('Grad', GradientBoostingClassifier(n_estimators=9, learning_rate=1.0, max_depth=2, random_state=9)))
        #models.append(('SVM',  SVC(probability=True,random_state=9)))
        models.append(('MLPC', MLPClassifier(alpha=1)))
        
        self.models = sorted(models, key=lambda tup:tup[0])
        
        return self.models
    pass
    
    def load_learned_models(self):
        """
        load existing learned model from '../result/texture-classifier-*'
        """
    
        folder = '../result/texture-classifier-'
        filenames = glob.glob(folder+'*')
        models = []
        
        for filename in filenames:
            name = filename[len(folder):-4]
            model = joblib.load(filename)
            data = (name,model)
            models.append(data)
        pass
        
        self.models = sorted(models, key=lambda tup:tup[0])
        
        return self.models
    pass
    
    def load_h5f_data(self):
        h5f_data = h5py.File('../result/texture-data.h5', 'r')
        global_features = np.array(h5f_data['dataset_1'])
        h5f_data.close()
        return np.array(global_features)
    pass
    
    def load_h5f_label(self):
        h5f_label = h5py.File('../result/texture-labels.h5', 'r')
        global_labels = np.array(h5f_label['dataset_1'])
        h5f_label.close()
        return np.array(global_labels)
    pass
pass