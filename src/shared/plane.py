import numpy as np
import math
import cv2
import cython

# --------------------------------------------------------

def thresholdAndMorph(src, element_dilate, element_erode):
    """
    @src threshold it with cv::ADAPTIVE_THRESH_GAUSSIAN_C and the threshold value 200 (empiricaly tested) to @threshold_output
    @threshold_output apply a cv::dilate and cv::erode at the end to reconnect the head from the corps
    """

    if src.shape[2] != 1:
        src_gray = cv2.cvtColor(src, cv2.COLOR_RGB2GRAY)
    else:
        src_gray = src.copy()
    pass

    thresh = cv2.threshold(gray, 240, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C)
    thresh = cv2.dilate(thresh, element_dilate, iterations = 2)
    thresh = cv2.erode(thresh, element_erode, iterations = 2)
    
    return thresh
pass
    

# --------------------------------------------------------
    
def getErodeDilateMatrix(erosion_size, morph = cv2.MORPH_ELLIPSE):
    """
    calculate a structuring element matrix
    with cv::Size(2*erosion_size+1, 2*erosion_size+1)
    and cv::Point(erosion_size, erosion_size)
    @return cv::getStructuringElement
    @erosion_size the size on offset used by cv::getStructuringElement
    """
    
    mtx_size = erosion_size*2+1
    
    return cv2.getStructuringElement(
        morph, (mtx_size, mtx_size),
        (erosion_size, erosion_size)
    )
pass

# --------------------------------------------------------

@cython.boundscheck(False)
def findAreaAndFitEllipse(threshold_output):
    """
    select the best contours of the given threshold_output
    @contours is all contours founds by cv::findContours
    @minEllipse is all cv::fitEllipse when the contours[i] is valide
    @return the index of the best contours, or -1 if no one
    """

    contours = cv2.findContours(
        threshold_output,
        cv2.RETR_EXTERNAL,
        cv2.CHAIN_APPROX_NONE,
        (0,0)
    )
    
    max_area_size = 0.0
    max_area_index = -1
    minEllipse = []
    i = 0
    
    for cnt in contours:
    
        minRect = cv2.minAreaRect(cnt)
        
        if cnt.size() > 20 and max_area_size < cnt.size():
            ellipse = cv2.fitEllipse(cnt)
            minEllipse.append(ellipse)
            size = ellipse.size().widthellipse.size().height
            max_area_size = cnt.size();
            max_area_index = i;
        pass
        
        i += 1
    pass

    return [contours, minEllipse, max_area_index]
pass
    
# --------------------------------------------------------
