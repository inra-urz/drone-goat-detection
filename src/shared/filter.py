import numpy as np
import math
import cv2

from .color import *
from .shape import *
from .plane import *

def multiplane_blob_detection(detector, transformed, rsize, offset=4):
    """
    """
    dim = transformed.shape
    planes = cv2.split(transformed)
    planes = [cv2.cvtColor(i, cv2.COLOR_GRAY2BGR) for i in planes]
    planes.append(planes[0] + planes[2])
    planes.append(transformed)
    del planes[1]
    
    bbox_features = []
    
    for i,plane in enumerate(planes):
        keypoints = detector.detect(plane)
        
        for pt in keypoints:
            point = pt.pt
            size = 10
            
            x1 = abs(point[0] - size - offset)
            y1 = abs(point[1] - size - offset)
            x2 = min(point[0] + size + offset, dim[1])
            y2 = min(point[1] + size + offset, dim[0])
            
            if x2-x1 < rsize[0] or y2-y1 < rsize[0]:
                continue
            
            if x2-x1 > rsize[1] or y2-y1 > rsize[1]:
                continue
            
            bbox_features.append([x1,y1,x2,y2])
        pass
    pass
    
    return bbox_features
pass

def image_processing(src, config, only_detection):
    s1 = config['blur_size'][0]+1
    s2 = config['blur_size'][1]+1
    
    # the 2 next instruction increase activity recognition by 3%
    # used to enhance the edge of the goat
    if config['sharpen']:
        src = sharpen(src)
        
    # used to reduce the noise introduced by sharpen in the weed
    src = cv2.blur(src, (s1,s1))
    
    # reduce the weed and burned weed also increase goat contrast after saturation
    # also increase detection and activity by 1%
    if config['remove_yellow']:
        remove_yellow(src)
        
    # transform the color space to retrieve the Hue
    src = saturate(src)
    
    if only_detection:
        # this first bluring allow to remove weed noise
        src = cv2.blur(src, (s1,s1))
        
        # remove weed for edge detection (thresholding)
        # value found in goat histogram
        # but we have reduced the green threshold for dectection
        src = remove_green_major(src,
            -config['remove_green_thresh'][0],
            config['remove_green_max'][0],
        )
        
        # only need a huge blured blob for ellipse fetting
        # do not care about the activity recognition
        src = cv2.blur(src, (s2,s2))
    else:
        # propagate region to try to reconnect the goat neck
        # bad idea, this reduce the activity recognition by 2%
        # src = cv2.blur(src, (3,3))
        
        # unification of similar colors (region)
        # also increase detection and activity by 1%
        if config['bilateral']:
            src = cv2.bilateralFilter(src, 20, 75, 75)
            
        # propagate color inside region
        # usefull for leter when geting intersection (violet)
        # when to contour detection is sparse
        # also increase activity regognition by 4%
        src = cv2.blur(src, (s1,s1))
        
        # remove weed for edge detection (thresholding)
        # value found in goat histogram
        src = remove_green_major(src,
            config['remove_green_thresh'][1],
            config['remove_green_max'][1],
        )
    pass
        
    return src
pass