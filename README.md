# Hough transform for goat detection and activity anylisis

please make a build/ folder for compilation
``
mkdir build
cd build
cmake ..
make
``

activity parameter
``
additional parameter can be provided to load additional goat for activity recognition
--dir replace the default set/Black and set/Red folder loading by the next arg path
      ex: --dir set/Untested/ the loading is recursive
--nogui disable graphics user interface and key-waiting
--write save the displayed output to the sub-folder area/
--noout disable output execpt the satistics (faster classification performance analysis)
--csv display the stdout in csv format (usefull for statistics analysis)
``

example:
``
./activity --dir set/learning --write
./activity --dir set/testing/Eating --nogui --csv
./activity set/learning/Black/1_C.png set/learning/Red/2_D.png
``

detection parameter
``
additional parameter can be provided it's the image that represent the pasture how the goat should be detected
--nogui disable graphics user interface and key-waiting
--write save the displayed output to the sub-folder area/
--dir replace the default set/Black and set/Red folder loading by the next arg path
      ex: --dir set/Untested/ the loading is recursive
--csv display the stdout in csv format (usefull for statistics analysis)
``

example:
``
./detection --write set/pasture.png
./detection --dir set/database --nogui --csv set/1.tif set/2.tif set/3.tif set/13.tif set/18.tiff
``